// console.log("Heloo");

// [SECTION] While Loop

// If the condition evaluates to true, the statements inside the code blcok will executed

/*
	Syntax 
	while (expression/condition){
		statement
	}
*/

// While the value of count is not equal to 0


let count = 5;

while(count !== 0){
	console.log("While: " + count);

	// iteration - it increases the value of count after every iteration to stop the loop when it reaches 5

	// ++ - increment, -- decrement

	count --;
}


//================================================

// [SECTION] Do While Loop

// A do-while loop works a  lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

/*
	Syntax:

	do {
		statement
	} while (expression/condition)
*/

let number = Number(prompt("Give me a number"));

do {

	console.log("Do While: " + number);

	// controls the sequence of increments
	number += 1;

} while (number < 10)


//======================================

// [SECTION] For Loops

// A For Loop is more flexible than while and do-while loops.

/*
	Syntax
	for (initialization; expression/condition; finalExpression) {
		Statement
	}
*/

/*for (let count = 0; count <= 15; count++) {
		console.log(count);
}*/


let myString = prompt("give me a word");
// Characters in strings may be counted using the .length property

/*console.log(myString.length);*/

/*console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);*/

// will create a loop that will printout the individual letters of a variable

for (let x = 0; x < myString.length; x++)

	//the current value of my string printed out using it's index value
	console.log(myString[x]);



// Changing of vowels using loops

// if the characters of your name is a vowel letter, instead of displayung the character, display number "3"
let myName = "Christoper";

for (let i = 0; i < myName.length; i++) {
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 

		) {
			console.log("");
	} 
	else {
		console.log(myName[i]);
	}
}
